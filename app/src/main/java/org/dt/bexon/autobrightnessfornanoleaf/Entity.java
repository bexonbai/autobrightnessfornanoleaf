package org.dt.bexon.autobrightnessfornanoleaf;

public class Entity {
    public Brightness brightness;

    public Entity(Brightness brightness) {
        this.brightness = brightness;
    }

    public static class Brightness {
        public Integer value;

        public Brightness(Integer value) {
            this.value = value;
        }
    }
}
