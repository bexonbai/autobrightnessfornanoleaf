package org.dt.bexon.autobrightnessfornanoleaf;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.dt.bexon.autobrightnessfornanoleaf.service.BrightnessService;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getName();
    private PowerManager.WakeLock mWakeLock;

    @SuppressLint("BatteryLife") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            i.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
        }
        //data为应用包名
        i.setData(Uri.parse("package:" + getPackageName()));
        startActivity(i);
    }

    public void start(View view) {
        Log.d(TAG, "start: ");
        EditText editText = findViewById(R.id.urlEdit);
        String url = editText.getText().toString();
        Intent intent = new Intent(MainActivity.this, BrightnessService.class);
        intent.putExtra("url", url);
        startService(intent);
        BrightnessService.BrightnessService(this);
    }
}