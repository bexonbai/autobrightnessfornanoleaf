package org.dt.bexon.autobrightnessfornanoleaf.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class BrightnessService extends Service implements SensorEventListener, Interceptor {

    public static final String TAG = BrightnessService.class.getName();
    public static String url;

    private static SensorManager mSensorManager;
    private static Sensor mSensor;
    private static Context mContext;
    int bri = 0;
    private PowerManager.WakeLock wakeLock;

    public static void BrightnessService(Context context) {
        mContext = context;
        //第一步：通过getSystemService获得SensorManager实例对象
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        //第二步：通过SensorManager实例对象获得想要的传感器对象:参数决定获取哪个传感器
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    public BrightnessService() {

    }

    @Override public int onStartCommand(Intent intent, int flags, int startId) {
        url =intent.getStringExtra("url");
        acquireWakeLock();
        Log.d(TAG, "BrightnessService: " + url);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 传感器精度发生改变的回调接口
     */
    @Override public void onSensorChanged(SensorEvent event) {
        // 在传感器精度发生改变时做些操作，accuracy为当前传感器精度
        // 這裏以700为峰值 (辦公室白天開燈約600)
        float lux = event.values[0];
        Log.d(TAG, "onSensorChanged: " + lux);
        if (lux > 700) {
            lux = 700;
        }
        int v = (int) (100f / 700 * lux);
        Log.d(TAG, "brightness: " + v);
        if (bri != v) {
            bri = v;
            send(v);
        }
    }

    private void send(int val) {
        String json = "{ \"brightness\": {\"value\": "+val+"}}";
        final JSONObject jsonObject = new JSONObject();
        JSONObject brightness = new JSONObject();
        try {
            jsonObject.put("value", val);
            brightness.put("brightness", jsonObject);
        } catch (JSONException e) {
            // handle exception
        }
        //开启线程，发送请求
        new Thread(() -> {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addNetworkInterceptor(this)
                    .readTimeout(8, TimeUnit.SECONDS)                  //设置请求超时
                    .writeTimeout(8, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .build();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(brightness.toString(), JSON);
            Headers headers = new Headers.Builder()
                    .add("Content-Type", "application/json")
                    .add("User-Agent", "PostmanRuntime/7.26.8")
                    .add("Accept", "*/*")
                    .add("Accept-Encoding", "gzip, deflate, br")
                    .add("Connection", "keep-alive")
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .headers(headers)
                    .put(body) // PUT here.
                    .build();

            try {
                Response response = client.newCall(request).execute();
                Log.d(TAG, "send: " + response.code());
                // Do something with the response.
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * 传感器事件值改变时的回调接口：执行此方法的频率与注册传感器时的频率有关
     */
    @Override public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // 大部分传感器会返回三个轴方向x,y,x的event值，值的意义因传感器而异
        Log.d(TAG, "onAccuracyChanged: " + accuracy);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        mSensorManager.unregisterListener(this);
        releaseWakeLock();
    }

    @NotNull @Override public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request().newBuilder()
                .addHeader("Connection","close").build();
        return chain.proceed(request);
    }

    private void acquireWakeLock() {
        if (null == wakeLock) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK
                    | PowerManager.ON_AFTER_RELEASE, getClass()
                    .getCanonicalName());
            if (null != wakeLock) {
                // Log.i(TAG, "call acquireWakeLock");
                wakeLock.acquire();
            }
        }
    }
    // 释放设备电源锁
    private void releaseWakeLock() {
        if (null != wakeLock && wakeLock.isHeld()) {
            //Log.i(TAG, "call releaseWakeLock");
            wakeLock.release();
            wakeLock = null;
        }
    }
}